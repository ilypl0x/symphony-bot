# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 09:10:23 2020

@author: JVEGA1
"""
from ebaysdk.finding import Connection as finding
from bs4 import BeautifulSoup

ID_APP = 'JustinVe-ilypl0x-PRD-c69ec6ae1-833e75e0'

Keywords = 'pokemon mini console'
#Keywords = input('what are you searching for? (ex: white piano)\n')
api = finding(appid=ID_APP, config_file=None)
api_request = { 'keywords': Keywords,
			      'itemFilter': [ 
						   {'name':'Condition','value':['New','Used']},
						   {'name':'Currency','value':'USD'},
						   #{'name':'FreeShippingOnly','value': False},
						   {'name':'LocatedIn','value':'US'},
						   {'name':'MaxPrice','value': 100},
						   {'name':'ListingType','value': ['Auction','FixedPrice','AuctionWithBIN']}
						   #{'name':'MaxPrice','value': 50},
						   #{'name':'LocatedIn','value':'US'},
						   #{'name':'MaxPrice','value': 50},
									   ],
				  'sortOrder':'PricePlusShippingLowest'}
response = api.execute('findItemsByKeywords', api_request)
json_resp =response.json()

print(json_resp)



soup = BeautifulSoup(response.content,'lxml')
totalentries = int(soup.find('totalentries').text)
items = soup.find_all('item')

for item in items:
    cat = item.categoryname.string.lower()
    title = item.title.string.lower()
    price = int(round(float(item.currentprice.string)))
    url = item.viewitemurl.string.lower()

    print('________')
    print('cat:\n' + cat + '\n')
    print('title:\n' + title + '\n')
    print('price:\n' + str(price) + '\n')
    print('url:\n' + url + '\n')
